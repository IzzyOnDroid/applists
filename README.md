[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](https://codeberg.org/IzzyOnDroid/.profile/src/branch/master/CODE_OF_CONDUCT.md)

This project is about the Android app listings at https://android.izzysoft.de/

There's no code maintained here. The main purpose of this project is to enable
you to actively contribute to the app listings (e.g. by suggesting apps that
should be added or removed, or reviews/links added to them), as well as to
reach out for help. Suggestions for other improvals are of course welcomed,
too.


## What are the IzzyOnDroid app listings?
The app listings at *IzzyOnDroid* shall help you finding apps specific to your
needs. For that, they are organized by „real-life categories“, ordered by their
resp. ratings, and enhanced by further details such as number of permissions
used, reviews and further links – hopefully making it easier to compare them.
The possibility to [search apps by category *and permission*][1] and get some
details on the meaning, purpose of [permissions][2] including their potential
dangers completes their feature set.

Currently, the applists support the following app sources:

* [Google Play Store][3]
* [F-Droid][4]
* [IzzyOnDroid repo][5]
* [Aptoide][6] (only the official, manually curated „Apps“ repo)
* [Xposed repo][7]


## How can I get my favorite app listed there?
This is what this project here at GitLab was set up for: use the issues to
propose new apps. Of course, first make sure the app you wish to add isn't
already there. Then, please use the template and fill in as many details as you
have for the app.

If you know additional ressources for an app already listed (e.g. you found a
good tutorial, or a helpful review), don't hesitate to suggest those as well!

If you are a developer and want **your** app listed – but haven't published it
yet: If it's [FOSS][8], you might want to first file a [Request for Packaging][9]
with the official F-Droid repository if it meets [their inclusion criteria][10],
or alternatively [with my F-Droid compatible repo][11] if it slightly fails them
but meets the slightly lighter ones with that.


## I have some issues with the site itself
Same game: file an issue. If the template drop-down doesn't show a suitable
template, just do it „free hand” after clearing the template.


## I have another suggestion for the site
Those are welcome as well, of course! Again, file an issue with a suiting
template (if such exists) or after „cleaning the plate“.


[IzzyOnDroid]: https://android.izzysoft.de/
[1]: https://android.izzysoft.de/applists/search "IzzyOnDroid: Search apps by category and permissions"
[2]: https://android.izzysoft.de/applists/perms "IzzyOnDroid: Permissions explained"
[3]: https://play.google.com/store/apps "Google Play Store Apps"
[4]: https://f-droid.org/ "F-Droid"
[5]: https://apt.izzysoft.de/fdroid/index/info "IzzyOnDroid App Repo"
[6]: https://en.aptoide.com/ "Aptoide App Store"
[7]: https://repo.xposed.info/
[8]: https://en.wikipedia.org/wiki/Free_and_open-source_software "Wikipedia: Free and open source software"
[9]: https://gitlab.com/fdroid/rfp/issues "F-Droid: Request for Packaging"
[10]: https://f-droid.org/wiki/page/Inclusion_Policy "F-Droid Inclusion Criteria"
[11]: https://gitlab.com/IzzyOnDroid/repo "IzzyOnDroid Repo Repo"
